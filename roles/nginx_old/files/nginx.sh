#!/usr/bin/env bash

export NGINX_PATH=/usr/local/nginx/sbin

export PATH=${PATH}:${NGINX_PATH}