#!/usr/bin/env bash

export OPENRESTY_PATH=/usr/local/openresty/bin

export PATH=${PATH}:${OPENRESTY_PATH}