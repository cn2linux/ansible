## extra_varas 参数说明

| 参数         | 默认值         | 说明                                                |
|------------|-------------|---------------------------------------------------|
| Acme       | no          | 是否安装acme.sh                                       |
| AcmeServer | letsencrypt | 指定acme 证书服务商 <br/> letsencrypt (默认) <br/> zerossl |
| AcmeEmail  | test@a.com  | acme.sh 所设置的邮箱地址                                  |

        