## extra_varas 参数说明

|   参数  | 默认值    | 说明                                                                              |
|-----|--------|---------------------------------------------------------------------------------|
|   Version  | go1.19 | go的版本号码                                                                         |
|   Publisher  | mirror  | 下载源地址<br/> official: go.dev<br/>mirror: mirrors.ustc.edu.cn<br/>国内服务器推荐： mirror |

