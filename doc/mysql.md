## extra_varas 参数说明

| 参数           | 默认值       | 说明                                                                                              |
|--------------|-----------|-------------------------------------------------------------------------------------------------|
| GetFlag      | A         | P: percona <br/>A: MySQL 社区版                                                                    |
| GetVersion   | v5        | v5: MySQL 5.7.x  <br/>v8: MySQL 8.0.x                                                           |
| Simple       | true      | true: my.conf 在/etc/ 下，数据在 /usr/local/mysql/data 下 <br/>false: 需要指定参数: DataRootDir、InstanceName |
| DataRootDir  | 数据库实例根路径： | /data                                                                                           |
| InstanceName | 数据库实例名称：  | mysql_3306_core                                                                                 |
| Port         | 数据库实例端口   | 3306                                                                                            |
| Password     | root 密码   | 123123                                                                                          |

------------